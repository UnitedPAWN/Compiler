
















stock GetPlayerJob(playerid) {
	return CallRemoteFunction("Tachomatr", "iii", 1, playerid, 0);
}

stock SetPlayerJob(playerid, job) {
	return CallRemoteFunction("Tachomatr", "iii", 2, playerid, job);
}

stock GetPlayerAdminLevel(playerid) {
	return CallRemoteFunction("Tachomatr", "iii", 3, playerid, 0);
}

stock SetPlayerAdminLevel(playerid, lvl) {
	return CallRemoteFunction("Tachomatr", "iii", 4, playerid, lvl);
}

stock GetPlayerJail(playerid) {
	return CallRemoteFunction("Tachomatr", "iii", 5, playerid, 0);
}

stock SetPlayerJail(playerid, lvl) {
	return CallRemoteFunction("Tachomatr", "iii", 6, playerid, lvl);
}

stock GetPlayerWarpBlock(playerid) {
	return CallRemoteFunction("Tachomatr", "iii", 7, playerid, 0);
}

stock SetPlayerWarpBlock(playerid, lvl) {
	return CallRemoteFunction("Tachomatr", "iii", 8, playerid, lvl);
}

stock PutPlayerInJail(playerid, time) {
    ResetPlayerWeapons(playerid);
	SetPlayerInterior(playerid, 0);
	SetPlayerWarpBlock(playerid, 1);
	switch(random(3)) {
		case 0: SetPlayerPos(playerid, 252.5713, 1800.3693, 7.4212);
		case 1: SetPlayerPos(playerid, 249.4624, 1800.6694, 7.4141);
		case 2: SetPlayerPos(playerid, 245.2964, 1800.8026, 7.4141);
	}
	SetPlayerJail(playerid, time);
	SetPlayerWantedLevel(playerid, 0);
	return 1;
}

stock ReleasePlayerFromJail(playerid, bool:instant = false) {
	if(instant) {
	    SetPlayerWantedLevel(playerid, 0);
		SetPlayerInterior(playerid, 0);
		SetPlayerPos(playerid, 2290.1501, 2432.0678, 10.8203);
		SetPlayerWarpBlock(playerid, 0);
		SetPlayerJail(playerid, -1);
	}
	else {
	    SetPlayerJail(playerid, 1);
	}
	return 1;
}

