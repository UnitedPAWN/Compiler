#include <a_samp>
#include <RouteConnector>
#include <streamer>

enum Trasa
{
    Destination,
    Objekty[1024],
    bool:Prepocitavani,
    Delka,
    GPS_Polygon,
    bool:IsInPolygon
};

new TrasaHrace[MAX_PLAYERS][Trasa];
/*
	native NavigateMeThere( playerid , Float:GPSX, Float:GPSY, Float:GPSZ );
	native GPS_OnPlayerLeaveDynamicArea(playerid, areaid);
	native GPS_OnPlayerEnterDynamicArea(playerid, areaid);
	native GPS_OnPlayerConnect(playerid);
	native GPS_OnPlayerDisconnect(playerid);
	native DisableGPS(playerid);

*/
#pragma dynamic 16777215

stock NavigateMeThere( playerid , Float:GPSX, Float:GPSY, Float:GPSZ )
{
    if(TrasaHrace[playerid][Prepocitavani])
    {
        SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}Pros�m po�kej chv�ly p�ed v�b�rem dal�� destinace!");
        return 1;
    }
    if(TrasaHrace[playerid][Destination] != -1)
    {
        DisableGPS(playerid);
    }
    
    new TempClosePlayer = NearestPlayerNode(playerid);
    new TempDestination = NearestNodeFromPoint(GPSX,GPSY,GPSZ);
    
    if(TempClosePlayer == -1)
    {
        SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}Chyba inicializace....");
        return 1;
    }
    if(TempDestination == -1)
    {
        SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}Chybn� destinace....");
        return 1;
    }
    if(CalculatePath(TempClosePlayer, TempDestination, playerid, .CreatePolygon = true, .GrabNodePositions = true))
    {
        TrasaHrace[playerid][Prepocitavani] = true;
        SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}P�ipojuji se k satelit�m.....");
    }
    else
    {
        SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}Chyba inicializace....");
    }
    return 1;
}

public OnPlayerClosestNodeIDChange(playerid,old_NodeID,new_NodeID)
{
    if(new_NodeID != -1)
    {
        if(TrasaHrace[playerid][Destination] != -1)
        {
            if(TrasaHrace[playerid][Destination] == new_NodeID)
            {
                SendClientMessage(playerid,0xFFFF00FF,"[GPS] {FFFFFF}Dorazil jsi do c�lov� destinace....");
                DisableGPS(playerid);
            }
            else
            {
                if(!TrasaHrace[playerid][IsInPolygon])
                {
                    if(!TrasaHrace[playerid][Prepocitavani])
                    {

                        if(CalculatePath(new_NodeID, TrasaHrace[playerid][Destination], playerid, .CreatePolygon = true, .GrabNodePositions = true))
                        {
                            DisableGPS(playerid);
                            TrasaHrace[playerid][Prepocitavani] = true;
                        }
                    }
                }
            }
        }
    }
    return 1;
}

stock GPS_OnPlayerConnect(playerid)
{
    TrasaHrace[playerid][Destination] = -1;
}

stock GPS_OnPlayerDisconnect(playerid)
{
    if(TrasaHrace[playerid][Destination] != -1)
    {
        DisableGPS(playerid);
    }
}

stock DisableGPS(playerid)
{
    if(TrasaHrace[playerid][Destination] != -1)
    {
        for(new i = 0; i < TrasaHrace[playerid][Delka]; ++i)
        {
            DestroyPlayerObject(playerid,TrasaHrace[playerid][Objekty][i]);
        }
        TrasaHrace[playerid][Delka] = 0;
        TrasaHrace[playerid][Destination] = -1;
        gps_RemovePlayer(playerid);
        DestroyDynamicArea(TrasaHrace[playerid][GPS_Polygon]);
        TrasaHrace[playerid][GPS_Polygon] = -1;
        TrasaHrace[playerid][IsInPolygon] = false;
    }
    return 1;
}


public GPS_WhenRouteIsCalculated(routeid,node_id_array[],amount_of_nodes,Float:distance,Float:Polygon[],Polygon_Size,Float:NodePosX[],Float:NodePosY[],Float:NodePosZ[])//Every processed Queue will be called here
{
    TrasaHrace[routeid][Prepocitavani] = false;
    if(amount_of_nodes > 1)
    {
        for(new i = 0; i < amount_of_nodes; ++i)
        {
        	TrasaHrace[routeid][Objekty][i] = CreatePlayerObject(routeid,1318,NodePosX[i],NodePosY[i],NodePosZ[i]+1.0,90.0,0.0,0.0,150.0);
		}
        TrasaHrace[routeid][Delka] = amount_of_nodes;
        TrasaHrace[routeid][Destination] = node_id_array[amount_of_nodes-1];
        TrasaHrace[routeid][GPS_Polygon] = CreateDynamicPolygon(Polygon,.maxpoints = Polygon_Size,.playerid = routeid);
        TrasaHrace[routeid][IsInPolygon] = IsPlayerInDynamicArea(routeid,TrasaHrace[routeid][GPS_Polygon]) == 1;
        gps_AddPlayer(routeid);
        SendClientMessage(routeid,0xFFFF00FF,"[GPS] {FFFFFF}P�epo��t�v�m....");
    }
    else
    {
        SendClientMessage(routeid,0xFFFF00FF,"[GPS] {FFFFFF}Satelit neodpov�d�");
    }
    return 1;
}

stock GPS_OnPlayerEnterDynamicArea(playerid, areaid)
{
    if(areaid == TrasaHrace[playerid][GPS_Polygon])
    {
        TrasaHrace[playerid][IsInPolygon] = true;
    }
    return 1;
}

stock GPS_OnPlayerLeaveDynamicArea(playerid, areaid)
{
    if(areaid == TrasaHrace[playerid][GPS_Polygon] && TrasaHrace[playerid][IsInPolygon])
    {
        if(!TrasaHrace[playerid][Prepocitavani])
        {
            new TempClosePlayer = NearestPlayerNode(playerid);
            if(TempClosePlayer != -1)
            {
                if(CalculatePath(TempClosePlayer, TrasaHrace[playerid][Destination], playerid, .CreatePolygon = true, .GrabNodePositions = true))
                {
                    DisableGPS(playerid);
                    TrasaHrace[playerid][Prepocitavani] = true;
                }
            }
        }
        TrasaHrace[playerid][IsInPolygon] = false;
    }
    return 1;
}
