








#include <unit>


remote SetPlayerCredits(playerid, amount);
remote GivePlayerCredits(playerid, amount);
remote GetPlayerCredits(playerid);
remote ResetPlayerCredits(playerid);

remote GivePIDCredits(pid, amount);
remote SetPIDCredits(pid, amount);
remote GetPIDCredits(pid);

remote SetPlayerTokens(playerid, amount);
remote GivePlayerTokens(playerid, amount);
remote GetPlayerTokens(playerid);
remote ResetPlayerTokens(playerid);

remote GivePIDTokens(pid, amount);
remote SetPIDTokens(pid, amount);
remote GetPIDTokens(pid);

