/*

		EXIS INCLUDE NEEDED TO START THE EXIS ADMINSCRIPT

*/

#if defined _exis_included
  #endinput
#endif

#define _exis_included

#include <exis>

//----------------------------------------------
#define dUserSetFLOAT(%1).( udb_UserSetFloat(%1,
#define dUserSetINT(%1).( udb_UserSetInt(%1,
#define dUserFLOAT(%1).( udb_UserFloat(%1,
#define dUserINT(%1).( udb_UserInt(%1,
#define dUserSet(%1).( udb_UserSet(%1,
#define dUser(%1).( udb_User(%1,
//----------------------------------------------


//----------------------------------------------
// 		DUB_D, DINI, DUTILS - by DracoBlue
//----------------------------------------------

stock udb_hash(buf[]) {
	new length=strlen(buf);
    new s1 = 1;
    new s2 = 0;
    new n;
    for (n=0; n<length; n++)
    {
       s1 = (s1 + buf[n]) % 65521;
       s2 = (s2 + s1)     % 65521;
    }
    return (s2 << 16) + s1;
}

stock udb_encode(nickname[]) {
  new tmp2[256];
  set(tmp2,nickname);
  tmp2=strreplace("_","_00",tmp2);
  tmp2=strreplace(";","_01",tmp2);
  tmp2=strreplace("!","_02",tmp2);
  tmp2=strreplace("/","_03",tmp2);
  tmp2=strreplace("\\","_04",tmp2);
  tmp2=strreplace("[","_05",tmp2);
  tmp2=strreplace("]","_06",tmp2);
  tmp2=strreplace("?","_07",tmp2);
  tmp2=strreplace(".","_08",tmp2);
  tmp2=strreplace("*","_09",tmp2);
  tmp2=strreplace("<","_10",tmp2);
  tmp2=strreplace(">","_11",tmp2);
  tmp2=strreplace("{","_12",tmp2);
  tmp2=strreplace("}","_13",tmp2);
  tmp2=strreplace(" ","_14",tmp2);
  tmp2=strreplace("\"","_15",tmp2);
  tmp2=strreplace(":","_16",tmp2);
  tmp2=strreplace("|","_17",tmp2);
  tmp2=strreplace("=","_18",tmp2);
  return tmp2;
}

stock udb_decode(nickname[]) {
  new tmp2[256];
  set(tmp2,nickname);
  tmp2=strreplace("_01",";",tmp2);
  tmp2=strreplace("_02","!",tmp2);
  tmp2=strreplace("_03","/",tmp2);
  tmp2=strreplace("_04","\\",tmp2);
  tmp2=strreplace("_05","[",tmp2);
  tmp2=strreplace("_06","]",tmp2);
  tmp2=strreplace("_07","?",tmp2);
  tmp2=strreplace("_08",".",tmp2);
  tmp2=strreplace("_09","*",tmp2);
  tmp2=strreplace("_10","<",tmp2);
  tmp2=strreplace("_11",">",tmp2);
  tmp2=strreplace("_12","{",tmp2);
  tmp2=strreplace("_13","}",tmp2);
  tmp2=strreplace("_14"," ",tmp2);
  tmp2=strreplace("_15","\"",tmp2);
  tmp2=strreplace("_16",":",tmp2);
  tmp2=strreplace("_17","|",tmp2);
  tmp2=strreplace("_18","=",tmp2);
  tmp2=strreplace("_00","_",tmp2);
  return tmp2;
}

stock udb_Exists(nickname[]) {
  new tmp2[256];
  format(tmp2,sizeof(tmp2),"exis/%s.cfg",udb_encode(nickname));
  return fexist(tmp2);
}

stock udb_UserSetInt(nickname[],key[],value) {
  new fname[256];
  format(fname,sizeof(fname),"exis/%s.cfg",udb_encode(nickname));
  return dini_IntSet(fname,key,value);
}

stock udb_UserInt(nickname[],key[]) {
  new fname[256];
  format(fname,sizeof(fname),"exis/%s.cfg",udb_encode(nickname));
  return dini_Int(fname,key);
}

stock udb_CheckLogin(nickname[],pwd[]) {
  new fname[256];
  format(fname,sizeof(fname),"exis/%s.cfg",udb_encode(nickname));
  if (udb_UserInt(nickname,"heslo")==udb_hash(pwd)) return true;
  return false;
}

stock udb_Create(nickname[],pwd[]) {
  if (udb_Exists(nickname)) return false;
  new fname[256];
  format(fname,sizeof(fname),"exis/%s.cfg",udb_encode(nickname));
  dini_Create(fname);
  udb_UserSetInt(nickname,"heslo",udb_hash(pwd));
  return true;
}

stock IsNumeric(const string[]) {
	new length=strlen(string);
	if (length==0) return false;
	for (new i = 0; i < length; i++) {
		if (
		(string[i] > '9' || string[i] < '0' && string[i]!='-' && string[i]!='+')
		|| (string[i]=='-' && i!=0)
		|| (string[i]=='+' && i!=0)
		) return false;
	}
	if (length==1 && (string[0]=='-' || string[0]=='+')) return false;
	return true;
}

stock StrToInt(string[]) {
  return strval(string);
}

stock SetPlayerMoney(playerid,penize)
{
	ResetPlayerMoney(playerid);
	GivePlayerMoney(playerid,penize);
}

stock fcopytextfile(oldname[],newname[]) {
	new File:ohnd,File:nhnd;
	if (!fexist(oldname)) return false;
	ohnd=fopen(oldname,io_read);
	nhnd=fopen(newname,io_write);
	new tmp4res[256];
	while (fread(ohnd,tmp4res)) {
		StripNewLine(tmp4res);
		format(tmp4res,sizeof(tmp4res),"%s\r\n",tmp4res);
		fwrite(nhnd,tmp4res);
	}
	fclose(ohnd);
	fclose(nhnd);
	return true;
}

stock StripNewLine(string[])
{
	new len = strlen(string);
	if (string[0]==0) return ;
	if ((string[len - 1] == '\n') || (string[len - 1] == '\r')) {
		string[len - 1] = 0;
		if (string[0]==0) return ;
		if ((string[len - 2] == '\n') || (string[len - 2] == '\r')) string[len - 2] = 0;
	}
}

ret_memcpy(source[],index=0,numbytes) {
	new tmp4[256];
	new i=0;
	tmp4[0]=0;
	if (index>=strlen(source)) return tmp4;
	if (numbytes+index>=strlen(source)) numbytes=strlen(source)-index;
	if (numbytes<=0) return tmp4;
	for (i=index;i<numbytes+index;i++) {
		tmp4[i-index]=source[i];
		if (source[i]==0) return tmp4;
	}
	tmp4[numbytes]=0;
	return tmp4;
}

stock set(dest[],source[]) {
	new count = strlen(source);
	new i=0;
	for (i=0;i<count;i++) {
		dest[i]=source[i];
	}
	dest[count]=0;
}

stock equal(str1[],str2[],bool:ignorecase) {
    if (strlen(str1)!=strlen(str2)) return false;
    if (strcmp(str1,str2,ignorecase)==0) return true;
    return false;
}

stock strreplace(trg[],newstr[],src[]) {
    new f=0;
    new s1[256];
    new tmp4[256];
    format(s1,sizeof(s1),"%s",src);
    f = strfind(s1,trg);
    tmp4[0]=0;
    while (f>=0) {
        strcat(tmp4,ret_memcpy(s1, 0, f));
        strcat(tmp4,newstr);
        format(s1,sizeof(s1),"%s",ret_memcpy(s1, f+strlen(trg), strlen(s1)-f));
        f = strfind(s1,trg);
    }
    strcat(tmp4,s1);
    return tmp4;
}

stock strlower(txt[]) {
	new tmp4[256];
	tmp4[0]=0;
	if (txt[0]==0) return tmp4;
	new i=0;
	for (i=0;i<strlen(txt);i++) {
		tmp4[i]=tolower(txt[i]);
	}
	tmp4[strlen(txt)]=0;
	return tmp4;
}

stock  dini_Exists(filename[]) {
	if (fexist(filename)) return true;
	return false;
}

stock  dini_Create(filename[]) {
	new File:fhnd;
	if (fexist(filename)) return false;
	fhnd=fopen(filename,io_write);
	fclose(fhnd);
	return true;
}

stock  dini_PRIVATE_ExtractKey(line[]) {
	new tmp3[256];
	tmp3[0]=0;
	if (strfind(line,"=",true)==-1) return tmp3;
	set(tmp3,strlower(ret_memcpy(line,0,strfind(line,"=",true))));
	return tmp3;
}

stock  dini_PRIVATE_ExtractValue(line[]) {
    new tmp3[256];
    tmp3[0]=0;
    if (strfind(line,"=",true)==-1) {
        return tmp3;
    }
    set(tmp3,ret_memcpy(line,strfind(line,"=",true)+1,strlen(line)));
    return tmp3;
}

stock  dini_Set(filename[],key[],value[]) {
	new File:fohnd, File:fwhnd;
	new bool:wasset=false;
	new tmp3res[256];
	if (key[0]==0) return false;
	format(tmp3res,sizeof(tmp3res),"%s.part",filename);
	fohnd=fopen(filename,io_read);
	if (!fohnd) return false;
	fremove(tmp3res);
	fwhnd=fopen(tmp3res,io_write);
	while (fread(fohnd,tmp3res)) {
		StripNewLine(tmp3res);
		if ((!wasset)&&(equal(dini_PRIVATE_ExtractKey(tmp3res),key,true))) {
			format(tmp3res,sizeof(tmp3res),"%s=%s",key,value);
			wasset=true;
		}
		fwrite(fwhnd,tmp3res);
		fwrite(fwhnd,"\r\n");
	}

	if (!wasset) {
		format(tmp3res,sizeof(tmp3res),"%s=%s",key,value);
		fwrite(fwhnd,tmp3res);
		fwrite(fwhnd,"\r\n");
	}

	fclose(fohnd);
	fclose(fwhnd);

	format(tmp3res,sizeof(tmp3res),"%s.part",filename);
	if (fcopytextfile(tmp3res,filename)) {
		return fremove(tmp3res);
	}
	return false;
}

stock  dini_IntSet(filename[],key[],value) {
   new valuestring[256];
   format(valuestring,sizeof(valuestring),"%d",value);
   return dini_Set(filename,key,valuestring);
}

stock  dini_Int(filename[],key[]) {
   return strval(dini_Get(filename,key));
}

stock  dini_Get(filename[],key[]) {
	new File:fohnd;
	new tmp3res[256];
	new tmp3res2[256];
	tmp3res[0]=0;
	fohnd=fopen(filename,io_read);
	if (!fohnd) return tmp3res;
	while (fread(fohnd,tmp3res)) {
		StripNewLine(tmp3res);
		if (equal(dini_PRIVATE_ExtractKey(tmp3res),key,true)) {
			tmp3res2[0]=0;
			strcat(tmp3res2,dini_PRIVATE_ExtractValue(tmp3res));
			fclose(fohnd);
			return tmp3res2;
		}
	}
	fclose(fohnd);
	return tmp3res;
}
