#if !defined ____sprintf
#define ____sprintf

#if !defined SPRINTF_MAX_STRING
	#define SPRINTF_MAX_STRING 4096
#endif
#if !defined SPRINTF_DEBUG_STRING
	#define SPRINTF_DEBUG_STRING "[sprintf debug] '%s'[%d]"
#endif

#assert SPRINTF_MAX_STRING > 2

new
	_s@T[SPRINTF_MAX_STRING];

#if defined SPRINTF_DEBUG
	new	const _s@V[] = SPRINTF_DEBUG_STRING;
	#define sprintf(%1) (format(_s@T, SPRINTF_MAX_STRING, %1), printf(_s@V, _s@T, strlen(_s@T)), _s@T)
#else
	#define sprintf(%1) (format(_s@T, SPRINTF_MAX_STRING, %1), _s@T)
#endif

#endif