



/*
	REGISTER BANK TYPE ID
	
	1       Player Account
	2       Access Account
	3       Password Account
	4-100   Reserved
	110     Gang System by WaterSmoke

*/


#include <unit>


/*
native CreateCustomBank(owner, type, const name[] = "");
native HavePlayerBank(playerid, amount);
native HavePlayerBankStr(playerid, const amount[]);
native GivePlayerBank(playerid, amount);
native GivePlayerBankStr(playerid, const amount[]);
native SetPlayerBank(playerid, amount);
native SetPlayerBankStr(playerid, const amount[]);
native GetPlayerBankAcc(playerid);
native ResetPlayerBank(playerid);
native TransferPlayerBank(sourceid, targetid, amount)
native TransferPlayerBankStr(sourceid, targetid, const amount[]);

native HaveCustomBank(owner, type, amount);
native HaveCustomBankStr(owner, type, const amount[]);
native GiveCustomBank(owner, type, amount);
native GiveCustomBankStr(owner, type, const amount[]);
native SetCustomBank(owner, type, amount);
native SetCustomBankStr(owner, type, const amount[]);
native GetCustomBankAcc(owner, type);
native ResetCustomBank(owner, type);

native HaveBank(acc, amount);
native HaveBankStr(acc, const amount[]);
native GiveBank(acc, amount);
native GiveBankStr(acc, const amount[]);
native SetBank(acc, amount);
native SetBankStr(acc, const amount[]);
native ResetBank(acc);
native TransferBank(sourceAcc, targetAcc, amount);
native TransferBankStr(sourceAcc, targetAcc, const amount[]);
native ShowBankDialog(playerid, acc);

native GetBankAccount(owner, type);
native GetBankAccountPID(pid);
*/


remote CreateCustomBank(owner, type, const name[]);
remote HavePlayerBank(playerid, amount);
remote HavePlayerBankStr(playerid, const amount[]);
remote GivePlayerBank(playerid, amount);
remote GivePlayerBankStr(playerid, const amount[]);
remote SetPlayerBank(playerid, amount);
remote SetPlayerBankStr(playerid, const amount[]);
remote GetPlayerBankAcc(playerid);
remote ResetPlayerBank(playerid);
remote TransferPlayerBank(sourceid, targetid, amount);
remote TransferPlayerBankStr(sourceid, targetid, const amount[]);

remote HaveCustomBank(owner, type, amount);
remote HaveCustomBankStr(owner, type, const amount[]);
remote GiveCustomBank(owner, type, amount);
remote GiveCustomBankStr(owner, type, const amount[]);
remote SetCustomBank(owner, type, amount);
remote SetCustomBankStr(owner, type, const amount[]);
remote GetCustomBankAcc(owner, type);
remote ResetCustomBank(owner, type);

remote HaveBank(acc, amount);
remote HaveBankStr(acc, const amount[]);
remote GiveBank(acc, amount);
remote GiveBankStr(acc, const amount[]);
remote SetBank(acc, amount);
remote SetBankStr(acc, const amount[]);
remote ResetBank(acc);
remote TransferBank(sourceAcc, targetAcc, amount);
remote TransferBankStr(sourceAcc, targetAcc, const amount[]);
remote ShowBankDialog(playerid, acc);

remote GetBankAccount(owner, type);
remote GetBankAccountPID(pid);

