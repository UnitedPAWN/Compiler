


stock va_string(...) {
	return emit load.s.pri 12;
}

#define va_func(%0) VaFunc:(emit const.pri %0)

#define ReusableVarargCallerNative(%0) ReusableVarargCaller__(true, %0)
#define ReusableVarargCaller(%0) ReusableVarargCaller__(false, %0)

// FRM, RET, NUM, func, original_static_num_args, ...
stock ReusableVarargCaller__(bool:native_func, VaFunc:func, original_static_num_args, ...) {
	original_static_num_args *= 4;

	new stk, ret;
	new cip, i, rer;

	new original_num_args = emit(load.s.pri 0, add.c 8, load.i);
	new original_args = emit(load.s.pri 0, add.c 8, load.s.alt original_static_num_args, add);
	new original_args_end = emit(load.s.pri 0, add.c 8, load.s.alt original_num_args, add);

	new static_args_num = emit(load.s.pri 8) - 12;
	new static_args = emit(lctrl 5, add.c 20);
	new static_args_end = emit(lctrl 5, load.s.alt static_args_num, add, add.c 20);

	emit {
		lctrl 4
		stor.s.pri stk
	}

	while(original_args < original_args_end) {
		emit {
	        lref.s.pri original_args_end
			push.pri
	    }
	    original_args_end -= 4;
	}

	while(static_args < static_args_end) {
		emit {
	        lref.s.pri static_args_end
	        load.i
			push.pri
	    }
	    static_args_end -= 4;
	}

	emit {
		load.s.pri static_args_num
		load.s.alt original_num_args
		add
		load.s.alt original_static_num_args
		sub
		push.pri

		load.s.pri native_func
		jnz l_nat

		// Call the func because call.pri does not work
		lctrl 6
		add.c 28
		push.pri
		load.s.pri func
		sctrl 6

		stor.s.pri ret

		jump l_end
l_nat:
		load.s.pri func
		sysreq.pri
		stor.s.pri ret
l_end:

		load.s.pri stk
		sctrl 4
	}
	return ret;
}








