//
//              unIt include
//
//  Autor:		xhunterx
//  Version:    0.2c
//  Credits:    Y_Less
//
//  Don't edit this unless you are a pro and understand internal working of this system!
//  Neupravujte tento subor ak nieste profik a nerozumiete jeho vnutornemu fungovaniu!
//

#if !defined SCRIPT_PREFIX
	#error "Napis pred #include <unit>: #define SCRIPT_PREFIX(%1) MENO_SCRIPTU%1     a MENO_SCRIPTU zmen na nazov tvojho FS alebo modu."
#endif

/*#if !defined YSI_IS_CLIENT
	#define YSI_IS_CLIENT
	#include <YSI\y_master>
	#include <YSI\y_dialog>
#endif*/

//#include <YSI_Coding\y_hooks>

static Unit_FS_ID = -1;

//-----------------------|
// (Do NOT edit numbers)
#define NUM_GROUPS      10

#define GROUP_USER      0
#define GROUP_HRAC      0
#define GROUP_PREMIUM   1
#define GROUP_VIP       2
#define GROUP_EVENTER   3
#define GROUP_HELPER    4
#define GROUP_ADMIN     5
#define GROUP_MASTER    6
#define GROUP_COOWNER   7
#define GROUP_OWNER     8
#define GROUP_PAWNER    9
//----------------------|

static const GroupNames[NUM_GROUPS][32] =
{
	{"Hrac"},
	{"Premium"},
	{"VIP Hrac"},
	{"Eventer"},
	{"Helper"},
	{"Admin"},
	{"Master"},
	{"Spolumajitel"},
	{"Majitel"},
	{"Pawner"}
};

/*
native GetPlayerGroup(playerid);
native GetGroupName(id);
native GetGroupNameByID(id);
native IsPlayerInJail(playerid);
native IsPlayerMuted(playerid);
native IsPlayerFreezed(playerid);
native GetPlayerVars(playerid);
native VarPlayer(playerid);
native UnitObtainDialogID();
native UnitFreeDialogID(dialogid);
native CanPlayerTeleport(playerid);

native GivePlayerPremium(playerid);
native GivePlayerVIP(playerid, time = -1);
*/

stock GetPlayerGroup(playerid)
{
	return GetPVarInt(playerid, "unit_Level");
	//return CallRemoteFunction("GetPlayerGroup", "i", playerid);
}
/*
Vrati skupinu v ktorej je dany hrac.
Priklad:
  if(GetPlayerGroup(playerid) >= GROUP_ADMIN)
  {
  	  ...
  }
*/
stock GetGroupName(playerid)
{
    return GroupNames[GetPVarInt(playerid, "unit_Level")];
}
/*
Vrati string s menom skupiny, v ktorej sa hrac nachadza.
Priklad:
	new string[128];
	format(string,sizeof(string),"%s %s vyhodil %s %s z povolania",GetGroupName(playerid),Meno(playerid),GetGroupName(CMD_ID),Meno(CMD_ID));
	SendClientMessageToAll(B_BILA,string);
*/
stock GetGroupNameByID(id)
{
	return GroupNames[id];
}
/*
Vrati string s menom skupiny, ktora ma dane id.
Priklad:
	new string[128];
	format(string,sizeof(string),"Musis byt %s aby si mohol pouzit tento prikaz.",GetGroupNameByID(GROUP_ADMIN));
	SendClientMessageToAll(B_BILA,string);

Tento prikaz nema prilis prakticke vyuzitie :D
*/
stock IsPlayerInJail(playerid)
{
	return CallRemoteFunction("IsPlayerInJail", "i", playerid);
}
/*
Vrati 1 ak je hrac vo vezeni, inak vrati 0
Priklad:
  if(IsPlayerInJail(playerid)
  {
	  ...
  }
*/
stock IsPlayerMuted(playerid)
{
    return CallRemoteFunction("IsPlayerMuted", "i", playerid);
}
/*
Vrati 1 ak je hrac umlcany, inak vrati 0
Priklad:
  if(IsPlayerMuted(playerid))
  {
	  ...
  }
*/
stock IsPlayerFreezed(playerid)
{
    return CallRemoteFunction("IsPlayerFreezed", "i", playerid);
}
/*
Vrati 1 ak je hrac zamrazeny, inak vrati 0
Priklad:
  if(IsPlayerFreezed(playerid))
  {
	  ...
  }
*/
stock GetPlayerVars(playerid)
{
    return CallRemoteFunction("GetPlayerVars", "i", playerid);
}
/*
Vrati pocet varov ktore ma hrac
Priklad:
  new string[128];
  format(string,sizeof(string),"Hrac %s ma %d napomenuti.",Jmeno(playerid),GetPlayerVars(playerid));
  SendClientMessageToAll(B_BILA,string);
*/
stock VarPlayer(playerid)
{
    return CallRemoteFunction("VarPlayer", "i", playerid);
}
/*
Prida hracovy napomenutie a ak ma 3 tak ho kickne.
Vrati 2 ak je hrac kicknuty inak vrati 1
Priklad:
  if(VarPlayer(playerid) == 2) SendClientMessageToAllEx(B_CERV,"Hrac %s bol kicknuty za ...",Jmeno(playerid));
  else SendClientMessageToAllEx(B_CERV,"Hrac %s bol napomenuty za ...",Jmeno(playerid));

Nikdy nepouzivajte VarPlayer viackrat v if pretoze hrac dostane viacero napomenuti!
Ak potrebujete viacero podmienok pouzite switch
  switch(VarPlayer(playerid))
  {
	 case 1: SendClientMessageToAllEx(B_CERV,"Hrac %s bol napomenuty za ...",Jmeno(playerid));
	 csae 2: SendClientMessageToAllEx(B_CERV,"Hrac %s bol kicknuty za ...",Jmeno(playerid));
  }
alebo si ulozte vysledok:
  new kick = VarPlayer(playerid);
  if(kick == 1) ...
  if(kick == 2) ...
*/
stock IsReklama(text[])
{
    return CallRemoteFunction("IsReklama", "s", text);
}
/*
Skontroluje jestli je text reklama.
Vrati true jestli je a false jestli neni.
Priklad pouziti:
OnPlayerPrivMsg(playerid,text[])
{
    if(IsReklama(text))
	{
		Kick(playerid);
		return 0;
	}
}
*/
stock UnitBan(playerid, admin[] = "Neznamy", duvod[] = "Nezadany")
{
	return CallRemoteFunction("UnitBan", "iss", playerid, admin, duvod);
}
/*
Zabanuje hrace. Admin je jmeno admina, ktery mu dal ban. Je li ban automaticky, pouzite nazev sveho FS/modu. Duvod je duvod banu.
Priklad:
    UnitBan(playerid,"Duel","Ruseni duealu");
*/
stock  UnitTimeBan(playerid, minut, hodin, dni, admin[], duvod[])
{
    return CallRemoteFunction("UnitBan", "iiiiss", playerid, minut, hodin, dni, admin, duvod);
}
/*
Zabanuje hrace na urcity cas. Admin je jmeno admina, ktery mu dal ban. Je li ban automaticky, pouzite nazev sveho FS/modu. Duvod je duvod banu.
minut, hodin, dni je cas banu. Cas se spocita. Napriklad jestli zadate 2 hidiny 80 minut, hrac dostane ban na 3 hodiny 20 minut.
Nechcete li pouzit nektery z casu, zadejte 0.
Priklad banu na 2 a pul hodiny:
	UnitTimeBan(playerid,30,2,0,"Duel","Ruseni duealu");
To same je:
	UnitTimeBan(playerid,150,0,0,"Duel","Ruseni duealu");
A to same je:
	UnitTimeBan(playerid,90,1,0,"Duel","Ruseni duealu");
*/
stock bool:UnitIsReklama(playerid,text[])
{
	if(IsReklama(text))
	{
		new h_str[128], h_name[MAX_PLAYER_NAME+1];
		GetPlayerName(playerid, h_name, sizeof(h_name));
		format(h_str,sizeof(h_str),"%s %s bol zabanovany za reklamu.",GetGroupName(playerid),h_name);
		SendClientMessageToAll(0xF60000AA,h_str);
		UnitBan(playerid,"unit","Reklama");
		return true;
	}
	return false;
}
/*
Skontroluje, jestli je text reklama, a pokud ano, zabanuje hrace playerid, napise spravu hracum a vrati true.
Jinak vrati false.
Priklad pouziti:
	if(UnitIsReklama(playerid, text))
	{
		Pocet_Reklam++;
		return 0;
	}
ALEBO
	if(UnitIsReklama(playerid, cmdtext)) return 1;
*/
#define UnitReklama(%1); if(UnitIsReklama(%1)) return 0;
/*
Skontroluje text na reklamu, zabanuje hraca, prerusi vykonanie funkcie, a to vsetko v jednom.
Priklad:
	UnitReklama(playerid, text);
*/
stock UnitObtainDialogID()
{
    return CallRemoteFunction("UnitObtainDialogID", "");
}
/*
	Vrati volne ID dialogu.
	Napr:

	new dialog_warp;
	public OnFilterScriptInit()
	{
		dialog_warp = UnitObtainDialogID();
	}
	...
	DialogShow(playerid, dialog_warp, ...);
*/

stock  UnitFreeDialogID(dialogid)
{
    return CallRemoteFunction("UnitFreeDialogID", "i", dialogid);
}

/*
	Uvolni ID dialogu pre dalsie pouzitie.
	Napr:
	OnFilterScriptExit()
	{
		UnitFreeDialogID(dialog_warp);
	}
*/

stock CanPlayerTeleport(playerid, Float:x = 0.0, Float:y = 0.0, Float:z = 0.0, Int = 0)
{
    return CallRemoteFunction("UnitCanPlayerTeleport", "ifffi", playerid, x, y, z, Int);
}

/*
	Zisti, ci sa hrac moze teleportovat. Napriklad ci nieje vo vezeni a podobne.
	Napr:
	CMD:teleport(playerid, params[])
	{
	    if(CanPlayerTeleport(playerid))
	    {
			TeleportPlayer(playerid);
		}
		else SendClientMessage(playerid, -1, "Nemuzes se teleportovat");
	}
*/

stock GivePlayerPremium(playerid)
{
	return CallRemoteFunction("UnitGivePremium", "i", playerid);
}

/*
	Hraca alebo VIP zmeni na Premiuma. Nemoze spravit Premiuma z eventerov a vyssie.
	Napr:
	public OnPlayerRegister(playerid)
	{
	    GivePlayerPremium(playerid);
	}
*/

stock GivePlayerVIP(playerid, time = -1)
{
	return CallRemoteFunction("UnitGiveVIP", "ii", playerid, time);
}

/*

*/

//===============================================| Hooky |=====================================================|
#define PutPlayerInVehicle(%0) PutInVehicle(%0)

stock PutInVehicle(playerid, vehicleid, seatid)
{
	return CallRemoteFunction("PutInVehicle", "iii", playerid, vehicleid, seatid);
}

//===============================================| Interne |===================================================|
#if defined _ALS_OnPlayerText
	#undef OnPlayerText
#else
	#define _ALS_OnPlayerText
#endif

enum (<<=1)
{
	CB_OPT = 1,
	CB_OPW,
	CB_OPSC,
	CB_OPR,
	CB_OPL
}

forward DelayedInit();
forward SCRIPT_PREFIX(_OPT)(playerid, text[]);
forward SCRIPT_PREFIX(_OPW)(playerid, Float:x, Float:y, Float:z, Int);
forward SCRIPT_PREFIX(_OPSC)(playerid, model, Float:x, Float:y, Float:z, Int);
forward SCRIPT_PREFIX(_OPR)(playerid);
forward SCRIPT_PREFIX(_OPL)(playerid, group);

#define OnPlayerText(%1) 				SCRIPT_PREFIX(_OPT)(%1)
#define OnPlayerWarp(%1)				SCRIPT_PREFIX(_OPW)(%1)
#define OnPlayerSpawnCar(%1)  			SCRIPT_PREFIX(_OPSC)(%1)
#define OnPlayerLoginInUnit(%1)       	SCRIPT_PREFIX(_OPL)(%1)
#define OnPlayerRegisterInUnit(%1)    	SCRIPT_PREFIX(_OPR)(%1)

unit_OnScriptInit()
{
	#if !defined UNIT_NO_DELAY
	    SetTimer(#DelayedInit, 500, false);
	#else
		DelayedInit();
	#endif
	return 1;
}

public DelayedInit()
{
	new tmp = 0;
	if(funcidx(#SCRIPT_PREFIX(_OPT)) 	!= -1) tmp |= CB_OPT;
	if(funcidx(#SCRIPT_PREFIX(_OPW)) 	!= -1) tmp |= CB_OPW;
	if(funcidx(#SCRIPT_PREFIX(_OPSC)) 	!= -1) tmp |= CB_OPSC;
	if(funcidx(#SCRIPT_PREFIX(_OPL)) 	!= -1) tmp |= CB_OPL;
	if(funcidx(#SCRIPT_PREFIX(_OPR)) 	!= -1) tmp |= CB_OPR;
    Unit_FS_ID = CallRemoteFunction("UnitScriptReg", "si", #SCRIPT_PREFIX(), tmp);
	#if defined Unit_InitScript
	CallLocalFunction(#Unit_InitScript, "");
	#endif
	return 1;
}

unit_OnScriptExit()
{
    CallRemoteFunction("UnitScriptUnReg","i",Unit_FS_ID);
    return 1;
}












#if defined FILTERSCRIPT
//------------------------------------------|
public OnFilterScriptInit()
{
    unit_OnScriptInit();
    return 1;
}
#if defined _ALS_OnFilterScriptInit
    #undef OnFilterScriptInit
#else
    #define _ALS_OnFilterScriptInit
#endif
#define OnFilterScriptInit Unit_InitScript
#if defined Unit_InitScript
    forward Unit_InitScript();
#endif
//------------------------------------------|
#else
	#if defined GAMEMODE
//------------------------------------------|
public OnGameModeInit()
{
    unit_OnScriptInit();
    return 1;
}
#if defined _ALS_OnGameModeInit
    #undef OnGameModeInit
#else
    #define _ALS_OnGameModeInit
#endif
#define OnGameModeInit Unit_InitScript
#if defined Unit_InitScript
    forward Unit_InitScript();
#endif
//------------------------------------------|
	#else
#error Use #define FILTERSCRIPT or #define GAMEMODE
	#endif
#endif

#if defined FILTERSCRIPT
//------------------------------------------|
public OnFilterScriptExit()
{
    unit_OnScriptExit();
    #if defined Unit_ExitScript
    Unit_ExitScript();
    #endif
    return 1;
}
#if defined _ALS_OnFilterScriptExit
    #undef OnFilterScriptExit
#else
    #define _ALS_OnFilterScriptExit
#endif

#define OnFilterScriptExit Unit_ExitScript
#if defined Unit_ExitScript
    forward Unit_ExitScript();
#endif
//------------------------------------------|
#else
	#if defined GAMEMODE
//------------------------------------------|
public OnGameModeExit()
{
    unit_OnScriptExit();
    #if defined Unit_ExitScript
    Unit_ExitScript();
    #endif
    return 1;
}
#if defined _ALS_OnGameModeExit
    #undef OnGameModeExit
#else
    #define _ALS_OnGameModeExit
#endif
#define OnGameModeExit Unit_ExitScript
#if defined Unit_ExitScript
    forward Unit_ExitScript();
#endif
//------------------------------------------|
	#else
#error Use #define FILTERSCRIPT or #define GAMEMODE
	#endif
#endif
