/*

	=====   Text Draw  :::  Mini Functions   =====
			v0.1

		by [other]FreeWall
			Pawno.cz


Example :

1)
TD_Window(playerid,0x000000FF,3000,40,1);

2)
TD_Window(playerid,0x000000FF,3000,40,2);

3)
TD_Window(playerid,0x000000FF,3000,40,3);

4)
TD_Window(playerid,0x000000FF,3000,40,4);


*/
#include <a_samp>
#pragma tabsize 0
forward P_Window(playerid);
forward P_Window1(playerid);
//-----------------------|
enum TDTEXTDRAW
{
Text:Okno,
Text:Okno2,
Text:Okno3,
Text:Okno4
}
enum TDINFO
{
	druh1a,
	druh1b,
	druh2a,
	druh2b,
	timer1,
	barva1,
	barva2,
	druh2,
	casik
}
new TD[MAX_PLAYERS][TDTEXTDRAW];
new TDInfo[MAX_PLAYERS][TDINFO];
stock TD_Disconnect(playerid)
{
	TextDrawHideForPlayer(playerid,TD[playerid][Okno]);
	TextDrawHideForPlayer(playerid,TD[playerid][Okno2]);
	TextDrawHideForPlayer(playerid,TD[playerid][Okno3]);
	TextDrawHideForPlayer(playerid,TD[playerid][Okno4]);
	TextDrawDestroy(TD[playerid][Okno]);
	TextDrawDestroy(TD[playerid][Okno2]);
	TextDrawDestroy(TD[playerid][Okno3]);
	TextDrawDestroy(TD[playerid][Okno4]);
	KillTimer(TDInfo[playerid][timer1]);
}
//==============================================================================
stock TD_Window(playerid,barva,cas,cas2,druh)
{
	if(!IsPlayerConnected(playerid)) return 1;
	if(druh < 1) return 1;
	if(druh == 1){
		TDInfo[playerid][druh1a] = 0;
		TDInfo[playerid][druh1b] = 226;
		TDInfo[playerid][barva1] = barva;
		TDInfo[playerid][druh2] = druh;
		TDInfo[playerid][casik] = cas2;
		TextDrawDestroy(TD[playerid][Okno]);
		TextDrawDestroy(TD[playerid][Okno2]);
		TD[playerid][Okno] = TextDrawCreate(-10,TDInfo[playerid][druh1a], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno],0);
		TextDrawFont(TD[playerid][Okno],1);
		TextDrawUseBox(TD[playerid][Okno],1);
		TextDrawBoxColor(TD[playerid][Okno],TDInfo[playerid][barva1]);
		TextDrawTextSize(TD[playerid][Okno],660,300);
		TextDrawLetterSize(TD[playerid][Okno],2.0,1.9);
		TextDrawSetOutline(TD[playerid][Okno],1);
		TextDrawSetShadow(TD[playerid][Okno],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno]);
		//-----------------------------------------|
		TD[playerid][Okno2] = TextDrawCreate(-10,TDInfo[playerid][druh1b], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno2],0);
		TextDrawFont(TD[playerid][Okno2],1);
		TextDrawUseBox(TD[playerid][Okno2],1);
		TextDrawBoxColor(TD[playerid][Okno2],TDInfo[playerid][barva1]);
		TextDrawTextSize(TD[playerid][Okno2],660,600);
		TextDrawLetterSize(TD[playerid][Okno2],2.0,2.0);
		TextDrawSetOutline(TD[playerid][Okno2],1);
		TextDrawSetShadow(TD[playerid][Okno2],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno2]);
		KillTimer(TDInfo[playerid][timer1]);
		SetTimerEx("P_Window1",cas,0,"d",playerid);
	}
	else if(druh == 2){
		TDInfo[playerid][druh1a] = -250;
		TDInfo[playerid][druh1b] = 476;
		TDInfo[playerid][barva1] = barva;
		TDInfo[playerid][druh2] = druh;
		TDInfo[playerid][casik] = cas;
		TextDrawDestroy(TD[playerid][Okno]);
		TextDrawDestroy(TD[playerid][Okno2]);
		TD[playerid][Okno] = TextDrawCreate(-10,TDInfo[playerid][druh1a], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno],0);
		TextDrawFont(TD[playerid][Okno],1);
		TextDrawUseBox(TD[playerid][Okno],1);
		TextDrawBoxColor(TD[playerid][Okno],TDInfo[playerid][barva1]);
		TextDrawTextSize(TD[playerid][Okno],660,300);
		TextDrawLetterSize(TD[playerid][Okno],2.0,1.9);
		TextDrawSetOutline(TD[playerid][Okno],1);
		TextDrawSetShadow(TD[playerid][Okno],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno]);
		//-----------------------------------------|
		TD[playerid][Okno2] = TextDrawCreate(-10,TDInfo[playerid][druh1b], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno2],0);
		TextDrawFont(TD[playerid][Okno2],1);
		TextDrawUseBox(TD[playerid][Okno2],1);
		TextDrawBoxColor(TD[playerid][Okno2],TDInfo[playerid][barva1]);
		TextDrawTextSize(TD[playerid][Okno2],660,600);
		TextDrawLetterSize(TD[playerid][Okno2],2.0,2.0);
		TextDrawSetOutline(TD[playerid][Okno2],1);
		TextDrawSetShadow(TD[playerid][Okno2],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno2]);
		KillTimer(TDInfo[playerid][timer1]);
		TDInfo[playerid][timer1] = SetTimerEx("P_Window",cas2,1,"d",playerid);
	}
	else if(druh == 3){
		TDInfo[playerid][druh2a] = -8;
		TDInfo[playerid][druh2b] = 321;
		TDInfo[playerid][barva2] = barva;
		TDInfo[playerid][druh2] = druh;
		TDInfo[playerid][casik] = cas2;
		TextDrawDestroy(TD[playerid][Okno3]);
		TextDrawDestroy(TD[playerid][Okno4]);
		TD[playerid][Okno3] = TextDrawCreate(TDInfo[playerid][druh2a],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno3],0);
		TextDrawFont(TD[playerid][Okno3],1);
		TextDrawUseBox(TD[playerid][Okno3],1);
		TextDrawBoxColor(TD[playerid][Okno3],TDInfo[playerid][barva2]);
		TextDrawTextSize(TD[playerid][Okno3],TDInfo[playerid][druh2a]+325,600);
		TextDrawLetterSize(TD[playerid][Okno3],2.0,3.0);
		TextDrawSetOutline(TD[playerid][Okno3],1);
		TextDrawSetShadow(TD[playerid][Okno3],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno3]);
		//-----------------------------------------|
		TD[playerid][Okno4] = TextDrawCreate(TDInfo[playerid][druh2b],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno4],0);
		TextDrawFont(TD[playerid][Okno4],1);
		TextDrawUseBox(TD[playerid][Okno4],1);
		TextDrawBoxColor(TD[playerid][Okno4],TDInfo[playerid][barva2]);
		TextDrawTextSize(TD[playerid][Okno4],660,600);
		TextDrawLetterSize(TD[playerid][Okno4],2.0,3.0);
		TextDrawSetOutline(TD[playerid][Okno4],1);
		TextDrawSetShadow(TD[playerid][Okno4],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno4]);
		KillTimer(TDInfo[playerid][timer1]);
		SetTimerEx("P_Window1",cas,0,"d",playerid);
	}
	else if(druh == 4){
		TDInfo[playerid][druh2a] = -331;
		TDInfo[playerid][druh2b] = 650;
		TDInfo[playerid][barva2] = barva;
		TDInfo[playerid][druh2] = druh;
		TDInfo[playerid][casik] = cas;
		TextDrawDestroy(TD[playerid][Okno3]);
		TextDrawDestroy(TD[playerid][Okno4]);
		TD[playerid][Okno3] = TextDrawCreate(TDInfo[playerid][druh2a],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno3],0);
		TextDrawFont(TD[playerid][Okno3],1);
		TextDrawUseBox(TD[playerid][Okno3],1);
		TextDrawBoxColor(TD[playerid][Okno3],TDInfo[playerid][barva2]);
		TextDrawTextSize(TD[playerid][Okno3],-5,600);
		TextDrawLetterSize(TD[playerid][Okno3],2.0,3.0);
		TextDrawSetOutline(TD[playerid][Okno3],1);
		TextDrawSetShadow(TD[playerid][Okno3],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno3]);
		//-----------------------------------------|
		TD[playerid][Okno4] = TextDrawCreate(TDInfo[playerid][druh2b],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
		TextDrawAlignment(TD[playerid][Okno4],0);
		TextDrawFont(TD[playerid][Okno4],1);
		TextDrawUseBox(TD[playerid][Okno4],1);
		TextDrawBoxColor(TD[playerid][Okno4],TDInfo[playerid][barva2]);
		TextDrawTextSize(TD[playerid][Okno4],660,600);
		TextDrawLetterSize(TD[playerid][Okno4],2.0,3.0);
		TextDrawSetOutline(TD[playerid][Okno4],1);
		TextDrawSetShadow(TD[playerid][Okno4],1);
		TextDrawShowForPlayer(playerid,TD[playerid][Okno4]);
		KillTimer(TDInfo[playerid][timer1]);
		TDInfo[playerid][timer1] = SetTimerEx("P_Window",cas2,1,"d",playerid);
	}
	return 1;
}
//------------------------------------------------------------------------------
public P_Window1(playerid){
	if(!IsPlayerConnected(playerid)) return 1;
	if(TDInfo[playerid][druh2] == 1){
		KillTimer(TDInfo[playerid][timer1]);
		TDInfo[playerid][timer1] = SetTimerEx("P_Window",TDInfo[playerid][casik],1,"d",playerid);
	}
	else if(TDInfo[playerid][druh2] == 2){
		TextDrawHideForPlayer(playerid,TD[playerid][Okno]);
		TextDrawHideForPlayer(playerid,TD[playerid][Okno2]);
		TextDrawDestroy(TD[playerid][Okno]);
		TextDrawDestroy(TD[playerid][Okno2]);
	}
	else if(TDInfo[playerid][druh2] == 3){
		KillTimer(TDInfo[playerid][timer1]);
		TDInfo[playerid][timer1] = SetTimerEx("P_Window",TDInfo[playerid][casik],1,"d",playerid);
	}
	else if(TDInfo[playerid][druh2] == 4){
		TextDrawHideForPlayer(playerid,TD[playerid][Okno3]);
		TextDrawHideForPlayer(playerid,TD[playerid][Okno4]);
		TextDrawDestroy(TD[playerid][Okno3]);
		TextDrawDestroy(TD[playerid][Okno4]);
	}
	return 1;
}
//------------------------------------------------------------------------------
public P_Window(playerid){
	if(!IsPlayerConnected(playerid)) return 1;
	if(TDInfo[playerid][druh2] == 1){
		if(TDInfo[playerid][druh1a] > -250){
			TextDrawDestroy(TD[playerid][Okno]);
			TextDrawDestroy(TD[playerid][Okno2]);
			TD[playerid][Okno] = TextDrawCreate(-10,TDInfo[playerid][druh1a], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno],0);
			TextDrawFont(TD[playerid][Okno],1);
			TextDrawUseBox(TD[playerid][Okno],1);
			TextDrawBoxColor(TD[playerid][Okno],TDInfo[playerid][barva1]);
			TextDrawTextSize(TD[playerid][Okno],660,300);
			TextDrawLetterSize(TD[playerid][Okno],2.0,1.9);
			TextDrawSetOutline(TD[playerid][Okno],1);
			TextDrawSetShadow(TD[playerid][Okno],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno]);
			//-----------------------------------------|
			TD[playerid][Okno2] = TextDrawCreate(-10,TDInfo[playerid][druh1b], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno2],0);
			TextDrawFont(TD[playerid][Okno2],1);
			TextDrawUseBox(TD[playerid][Okno2],1);
			TextDrawBoxColor(TD[playerid][Okno2],TDInfo[playerid][barva1]);
			TextDrawTextSize(TD[playerid][Okno2],660,600);
			TextDrawLetterSize(TD[playerid][Okno2],2.0,2.0);
			TextDrawSetOutline(TD[playerid][Okno2],1);
			TextDrawSetShadow(TD[playerid][Okno2],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno2]);
			TDInfo[playerid][druh1a] --;
			TDInfo[playerid][druh1b] ++;
		} else {
			TextDrawHideForPlayer(playerid,TD[playerid][Okno]);
			TextDrawHideForPlayer(playerid,TD[playerid][Okno2]);
			TextDrawDestroy(TD[playerid][Okno]);
			TextDrawDestroy(TD[playerid][Okno2]);
			KillTimer(TDInfo[playerid][timer1]);
		}
	}
	else if(TDInfo[playerid][druh2] == 2){
		if(TDInfo[playerid][druh1a] <= 0){
			TextDrawDestroy(TD[playerid][Okno]);
			TextDrawDestroy(TD[playerid][Okno2]);
			TD[playerid][Okno] = TextDrawCreate(-10,TDInfo[playerid][druh1a], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno],0);
			TextDrawFont(TD[playerid][Okno],1);
			TextDrawUseBox(TD[playerid][Okno],1);
			TextDrawBoxColor(TD[playerid][Okno],TDInfo[playerid][barva1]);
			TextDrawTextSize(TD[playerid][Okno],660,300);
			TextDrawLetterSize(TD[playerid][Okno],2.0,1.9);
			TextDrawSetOutline(TD[playerid][Okno],1);
			TextDrawSetShadow(TD[playerid][Okno],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno]);
			//-----------------------------------------|
			TD[playerid][Okno2] = TextDrawCreate(-10,TDInfo[playerid][druh1b], "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno2],0);
			TextDrawFont(TD[playerid][Okno2],1);
			TextDrawUseBox(TD[playerid][Okno2],1);
			TextDrawBoxColor(TD[playerid][Okno2],TDInfo[playerid][barva1]);
			TextDrawTextSize(TD[playerid][Okno2],660,600);
			TextDrawLetterSize(TD[playerid][Okno2],2.0,2.0);
			TextDrawSetOutline(TD[playerid][Okno2],1);
			TextDrawSetShadow(TD[playerid][Okno2],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno2]);
			TDInfo[playerid][druh1a] ++;
			TDInfo[playerid][druh1b] --;
		} else {
			KillTimer(TDInfo[playerid][timer1]);
			SetTimerEx("P_Window1",TDInfo[playerid][casik],0,"d",playerid);
		}
	}
	else if(TDInfo[playerid][druh2] == 3){
		if(TDInfo[playerid][druh2a] > -331){
			TextDrawDestroy(TD[playerid][Okno3]);
			TextDrawDestroy(TD[playerid][Okno4]);
			TD[playerid][Okno3] = TextDrawCreate(TDInfo[playerid][druh2a],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno3],0);
			TextDrawFont(TD[playerid][Okno3],1);
			TextDrawUseBox(TD[playerid][Okno3],1);
			TextDrawBoxColor(TD[playerid][Okno3],TDInfo[playerid][barva2]);
			TextDrawTextSize(TD[playerid][Okno3],TDInfo[playerid][druh2a]+325,600);
			TextDrawLetterSize(TD[playerid][Okno3],2.0,3.0);
			TextDrawSetOutline(TD[playerid][Okno3],1);
			TextDrawSetShadow(TD[playerid][Okno3],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno3]);
			//-----------------------------------------|
			TD[playerid][Okno4] = TextDrawCreate(TDInfo[playerid][druh2b],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno4],0);
			TextDrawFont(TD[playerid][Okno4],1);
			TextDrawUseBox(TD[playerid][Okno4],1);
			TextDrawBoxColor(TD[playerid][Okno4],TDInfo[playerid][barva2]);
			TextDrawTextSize(TD[playerid][Okno4],660,600);
			TextDrawLetterSize(TD[playerid][Okno4],2.0,3.0);
			TextDrawSetOutline(TD[playerid][Okno4],1);
			TextDrawSetShadow(TD[playerid][Okno4],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno4]);
			TDInfo[playerid][druh2a] --;
			TDInfo[playerid][druh2b] ++;
		} else {
			TextDrawHideForPlayer(playerid,TD[playerid][Okno3]);
			TextDrawHideForPlayer(playerid,TD[playerid][Okno4]);
			TextDrawDestroy(TD[playerid][Okno3]);
			TextDrawDestroy(TD[playerid][Okno4]);
			KillTimer(TDInfo[playerid][timer1]);
		}
	}
	else if(TDInfo[playerid][druh2] == 4){
		if(TDInfo[playerid][druh2a] <= -5){
			TextDrawDestroy(TD[playerid][Okno3]);
			TextDrawDestroy(TD[playerid][Okno4]);
			TD[playerid][Okno3] = TextDrawCreate(TDInfo[playerid][druh2a],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno3],0);
			TextDrawFont(TD[playerid][Okno3],1);
			TextDrawUseBox(TD[playerid][Okno3],1);
			TextDrawBoxColor(TD[playerid][Okno3],TDInfo[playerid][barva2]);
			TextDrawTextSize(TD[playerid][Okno3],TDInfo[playerid][druh2a]+325,600);
			TextDrawLetterSize(TD[playerid][Okno3],2.0,3.0);
			TextDrawSetOutline(TD[playerid][Okno3],1);
			TextDrawSetShadow(TD[playerid][Okno3],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno3]);
			//-----------------------------------------|
			TD[playerid][Okno4] = TextDrawCreate(TDInfo[playerid][druh2b],-5, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~");
			TextDrawAlignment(TD[playerid][Okno4],0);
			TextDrawFont(TD[playerid][Okno4],1);
			TextDrawUseBox(TD[playerid][Okno4],1);
			TextDrawBoxColor(TD[playerid][Okno4],TDInfo[playerid][barva2]);
			TextDrawTextSize(TD[playerid][Okno4],660,600);
			TextDrawLetterSize(TD[playerid][Okno4],2.0,3.0);
			TextDrawSetOutline(TD[playerid][Okno4],1);
			TextDrawSetShadow(TD[playerid][Okno4],1);
			TextDrawShowForPlayer(playerid,TD[playerid][Okno4]);
			TDInfo[playerid][druh2a] ++;
			TDInfo[playerid][druh2b] --;
		} else {
			KillTimer(TDInfo[playerid][timer1]);
			SetTimerEx("P_Window1",TDInfo[playerid][casik],0,"d",playerid);
		}
	}
	return 1;
}