/*
 *
 * Quaternion Converter
 * Allows simple conversion from quaternion angles (supplied by new SA-MP 0.3b functions) to yaw/pitch/roll
 *
 * NAME         Quaternion Converter
 * AUTHOR		ev0lution
 * FILE         quaternion.inc
 * VERSION      1.0.0
 * LICENSE      GNU General Public License (see below)
 * COPYRIGHT    Copyright © 2010 ev0lution
 *
 *	This file is part of Quaternion Converter.
 *
 * 	Quaternion Converter is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 * 	Quaternion Converter is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Quaternion Converter.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

stock QuaternionToYawPitchRoll(Float:quat_w,Float:quat_x,Float:quat_y,Float:quat_z,&Float:x,&Float:y,&Float:z) {
	x = atan2(2*((quat_x*quat_y)+(quat_w+quat_z)),(quat_w*quat_w)+(quat_x*quat_x)-(quat_y*quat_y)-(quat_z*quat_z));
	y = atan2(2*((quat_y*quat_z)+(quat_w*quat_x)),(quat_w*quat_w)-(quat_x*quat_x)-(quat_y*quat_y)+(quat_z*quat_z));
	z = asin(-2*((quat_x*quat_z)+(quat_w*quat_y)));
	return 1;
}

stock QuaternionGetRoll(Float:quat_w,Float:quat_x,Float:quat_y,Float:quat_z,&Float:roll) {
	roll = atan2(2*((quat_x*quat_y)+(quat_w+quat_z)),(quat_w*quat_w)+(quat_x*quat_x)-(quat_y*quat_y)-(quat_z*quat_z));
	return 1;
}

stock QuaternionGetPitch(Float:quat_w,Float:quat_x,Float:quat_y,Float:quat_z,&Float:pitch) {
	pitch = atan2(2*((quat_y*quat_z)+(quat_w*quat_x)),(quat_w*quat_w)-(quat_x*quat_x)-(quat_y*quat_y)+(quat_z*quat_z));
	return 1;
}

stock QuaternionGetYaw(Float:quat_w,Float:quat_x,Float:quat_y,Float:quat_z,&Float:yaw) {
	yaw = asin(-2*((quat_x*quat_z)+(quat_w*quat_y)));
	return 1;
}